package com.example.profile;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.BitmapCompat;
import android.support.v7.app.AlertDialog;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.android.volley.NoConnectionError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;

import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.example.R;
import com.example.common.Const;
import com.example.common.NameUtil;
import com.example.common.NetworkUtil;
import com.example.common.PictureUtil;
import com.example.common.UIUtil;
import com.example.common.Utils;
import com.example.common.Validators;
import com.example.data.ArtefactType;
import com.example.data.ArtefactUtils;
import com.example.databinding.FragmentUserProfileBinding;
import com.example.network.ConfigEnvHolder;
import com.example.network.MyVolley;
import com.example.network.ROUTES;
import com.example.network.VolleyJSONRequestHelper;
import com.example.network.WebServiceCoordinator;
import com.example.user.UserDataPrefProvider;
import com.example.user.UserInfo;

import static junit.framework.Assert.assertTrue;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnUserProfileFragmentListener} interface
 * to handle interaction events.
 * Use the {@link UserProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserProfileFragment extends Fragment {
    public static final String FRAGMENT_TAG = UserProfileFragment.class.getSimpleName();

    private static final String LOG_TAG = UserProfileFragment.class.getSimpleName();

    private static final String KEY_FIRST_NAME = "name";
    private static final String KEY_LAST_NAME = "surname";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_PROFILE_PICTURE = "avatar";

    private static final int NAME_PARTS_COUNT = 2;

    private FragmentUserProfileBinding binding_;
    private UserInfo user_;

    private OnUserProfileFragmentListener listener_;

    private static final int REQUEST_CAMERA = 16;
    private static final int REQUEST_SELECT_FILE = 17;

    private static final String PROFILE_PICTURE_NAME_ON_SERVER = "profile_pic" + ArtefactUtils.getExtForType(ArtefactType.IMAGE);

    private static final String PICTURE_2_UPLOAD_FILE_NAME = "Vp_avatar" + ArtefactUtils.getExtForType(ArtefactType.IMAGE);

    private int userPictureWidth_;
    private int userPictureHeight_;

    private String profileImagePath_;
    private File profileImage_;

    private ResumableUpdateUserProfile updateUserProfileRequest_;
    private TextWatcher phoneNumberWatcher_;

    private TransferObserver fileUploadObserver_;

    private @NonNull ProfileEditableItems editedFields_ = new ProfileEditableItems();
//    private final Handler bkgTaskHandler_ = new Handler();

    public UserProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment UserProfileFragment.
     */
    public static UserProfileFragment newInstance() {
        return new UserProfileFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnUserProfileFragmentListener) {
            listener_ = (OnUserProfileFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnUserProfileFragmentListener");
        }
    }

    /**
     * Called to do initial creation of a fragment.  This is called after
     * {@link #onAttach(Activity)} and before
     * {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     * <p/>
     * <p>Note that this can be called while the fragment's activity is
     * still in the process of being created.  As such, you can not rely
     * on things like the activity's content view hierarchy being initialized
     * at this point.  If you want to do work once the activity itself is
     * created, see {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>Any restored child fragments will be created before the base
     * <code>Fragment.onCreate</code> method returns.</p>
     *
     * @param savedInstanceState If the fragment is being re-created from
     *                           a previous saved state, this is the state.
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        user_ = UserDataPrefProvider.getInstance(getContext()).loadData();
        phoneNumberWatcher_ = new PhoneNumberFormattingTextWatcher();

        updateUserProfileRequest_ = createUpdateProfileTask(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding_ = DataBindingUtil.inflate(inflater, R.layout.fragment_user_profile, container, false);

        binding_.saveChanges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding_.saveChanges.setEnabled(false);
                Map<String, String> params = new HashMap<>();
                if (!prepareParamsOrDisplayError(params)) {
                    return;
                }

                boolean isGoingToUpload = uploadImageIfNeeded(profileImage_, params);

                if (!isGoingToUpload) {
                    editedFields_ = updateUserDataLocal(params);

                    if (editedFields_.hasEdited()) {
                        // Send data on server
                        updateUserProfileRequest_.sendRequest(params);
                    }
                    else {
                        doExit();
                    }
                }
            }
        });

        binding_.pickUserImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createAndShowPickImageOptions(UserProfileFragment.this);
            }
        });

        return binding_.getRoot();
    }

    /**
     * Called immediately after {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}
     * has returned, but before any saved state has been restored in to the view.
     * This gives subclasses a chance to initialize themselves once
     * they know their view hierarchy has been completely created.  The fragment's
     * view hierarchy is not however attached to its parent at this point.
     *
     * @param view               The View returned by {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        userPictureWidth_ = getResources().getDimensionPixelSize(R.dimen.size_profile_picture);
        userPictureHeight_ = getResources().getDimensionPixelSize(R.dimen.size_profile_picture);

        binding_.phoneNumber.addTextChangedListener(phoneNumberWatcher_);

        // Update it because it might change when user sets new password
        user_.setToken(UserDataPrefProvider.getInstance(getContext()).loadAuthToken());

        updateUIControlsData();
    }

    /**
     * Called when the Fragment is visible to the user.  This is generally
     * tied to {@link Activity#onStart() Activity.onStart} of the containing
     * Activity's lifecycle.
     */
    @Override
    public void onStart() {
        super.onStart();

        updateUserProfileRequest_.onStart();
    }

    /**
     * Called when the Fragment is no longer started.  This is generally
     * tied to {@link Activity#onStop() Activity.onStop} of the containing
     * Activity's lifecycle.
     */
    @Override
    public void onStop() {
        updateUserProfileRequest_.onStop();

        super.onStop();
    }

    /**
     * Called when the view previously created by {@link #onCreateView} has
     * been detached from the fragment.  The next time the fragment needs
     * to be displayed, a new view will be created.  This is called
     * after {@link #onStop()} and before {@link #onDestroy()}.  It is called
     * <em>regardless</em> of whether {@link #onCreateView} returned a
     * non-null view.  Internally it is called after the view's state has
     * been saved but before it has been removed from its parent.
     */
    @Override
    public void onDestroyView() {
        binding_.phoneNumber.removeTextChangedListener(phoneNumberWatcher_);

        binding_ = null;
        super.onDestroyView();
    }

    /**
     * Called when the fragment is no longer in use.  This is called
     * after {@link #onStop()} and before {@link #onDetach()}.
     */
    @Override
    public void onDestroy() {
        phoneNumberWatcher_ = null;
        if (fileUploadObserver_ != null) {
            fileUploadObserver_.cleanTransferListener();
            fileUploadObserver_ = null;
        }
        updateUserProfileRequest_ = null;


        super.onDestroy();
    }

    @Override
    public void onDetach() {
        listener_ = null;

        super.onDetach();
    }


    /**
     * Receive the result from a previous call to
     * {@link #startActivityForResult(Intent, int)}.  This follows the
     * related Activity API as described there in
     * {@link Activity#onActivityResult(int, int, Intent)}.
     *
     * @param requestCode The integer request code originally supplied to
     *                    startActivityForResult(), allowing you to identify who this
     *                    result came from.
     * @param resultCode  The integer result code returned by the child activity
     *                    through its setResult().
     * @param data        An Intent, which can return result data to the caller
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d(LOG_TAG, "onActivityResult: " + requestCode);

        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        switch (requestCode) {
            case REQUEST_CAMERA: {
                if (!TextUtils.isEmpty(profileImagePath_)) {
                    final Bitmap newUserImage = PictureUtil.decodeSampledBitmapFromFile(profileImagePath_, userPictureWidth_, userPictureHeight_);
                    Log.d(LOG_TAG, "Bitmap from camera [" + newUserImage.getWidth() + ", " + newUserImage.getHeight() + "]\n"
                            + "\twith size: " + BitmapCompat.getAllocationByteCount(newUserImage));

                    binding_.userProfileImage.setImageBitmap(newUserImage);

                    binding_.userProfileInitials.setVisibility(View.GONE);
                    binding_.userProfileImage.setVisibility(View.VISIBLE);

                    profileImage_ = PictureUtil.saveImage2Storage(newUserImage, Bitmap.CompressFormat.JPEG, 100, getContext().getFilesDir(), PICTURE_2_UPLOAD_FILE_NAME);

                    // Add the original photo to gallery, not profileImage_ which contains smaller one
                    PictureUtil.galleryAddPicture(getContext(), profileImagePath_);

//                    Log.d(LOG_TAG, "Image file size is " + profileImage_.length() + " bytes");
                }
                else {
                    profileImage_ = null;
                }

                break;
            }

            case REQUEST_SELECT_FILE: {
                profileImagePath_ = PictureUtil.extractFilePathFromGallery(getContext(), data.getData());

                if (!TextUtils.isEmpty(profileImagePath_)) {
                    final Bitmap newUserImage = PictureUtil.decodeSampledBitmapFromFile(profileImagePath_, userPictureWidth_, userPictureHeight_);
                    Log.d(LOG_TAG, "Bitmap from camera [" + newUserImage.getWidth() + ", " + newUserImage.getHeight() + "]\n"
                            + "\twith size: " + BitmapCompat.getAllocationByteCount(newUserImage));

                    binding_.userProfileImage.setImageBitmap(newUserImage);

                    binding_.userProfileInitials.setVisibility(View.GONE);
                    binding_.userProfileImage.setVisibility(View.VISIBLE);

                    profileImage_ = PictureUtil.saveImage2Storage(newUserImage, Bitmap.CompressFormat.JPEG, 100, getContext().getFilesDir(), PICTURE_2_UPLOAD_FILE_NAME);
                    //PictureUtil.galleryAddPicture(getContext(), profileImage_);
                }
                else {
                    profileImage_ = null;
                }

                break;
            }

            default: {
                Log.w(LOG_TAG, "Unhandled request code " + requestCode);
            }
        }
    }// onActivityResult


    public @NonNull String getFragmentTag() {
        return FRAGMENT_TAG;
    }

    private void updateUIControlsData() {
        binding_.fullName.setText(getString(R.string.placeholder_2_space, user_.getFirstName(), user_.getLastName()));
        binding_.phoneNumber.setText(user_.getPhone());
        binding_.eMail.setText(user_.getEmail());

        final char[] initials = NameUtil.obtainInitials(user_.getFirstName(), user_.getLastName());
        binding_.userProfileInitials.setText(initials, 0, initials.length);

        final String awsServerUrl = ConfigEnvHolder.getInstance(getContext()).getS3ServerUrl();
        final String awsBucket = ConfigEnvHolder.getInstance(getContext()).getBucketName();

        final String URL_PROFILE_PICTURE_PATH = new Uri.Builder()
                .encodedPath(awsServerUrl)
                .appendEncodedPath(awsBucket)
                .appendEncodedPath(user_.getImagePath())
                .toString();

        MyVolley.getInstance(getContext()).getImageLoader().get(URL_PROFILE_PICTURE_PATH, getImageListener(binding_.userProfileImage, binding_.userProfileInitials));
    }

    private void doExit() {
        getActivity().onBackPressed();
    }

    private static ImageLoader.ImageListener getImageListener(@NonNull final ImageView view, @NonNull final TextView initialsView) {
        return new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if (response.getBitmap() == null) {
                    view.setVisibility(View.GONE);
                    initialsView.setVisibility(View.VISIBLE);
                } else {
                    initialsView.setVisibility(View.GONE);
                    view.setImageBitmap(response.getBitmap());
                    view.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                view.setVisibility(View.GONE);
                initialsView.setVisibility(View.VISIBLE);
            }
        };
    }

    private void createAndShowPickImageOptions(@NonNull final Fragment fragment) {
        new AlertDialog.Builder(fragment.getContext())
                .setTitle(R.string.title_image_picking)
                .setItems(R.array.dialog_items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0: {
                                // Taking photo - camera intent
                                final Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
                                    profileImagePath_ = dispatchTakePictureIntent(takePictureIntent);
                                }
                                else {
                                    UIUtil.showGravitatedToast(fragment.getContext(), R.string.msg_err_no_camera_intent, Toast.LENGTH_LONG, Gravity.CENTER, 0, 0);
                                }
                                break;
                            }

                            case 1: {
                                final Intent galleryBrowser = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                                        .setType("image/*");
                                fragment.startActivityForResult(Intent.createChooser(galleryBrowser, getString(R.string.title_select_image)), REQUEST_SELECT_FILE);
                                break;
                            }

                            default: {
                                dialog.dismiss();
                            }
                        }
                    }
                }).show();
    }

    private @NonNull String dispatchTakePictureIntent(@NonNull Intent intent) {
        File photoFile = PictureUtil.tryCreateTempFile(ArtefactUtils.getPrefForType(ArtefactType.IMAGE),
                ArtefactUtils.getExtForType(ArtefactType.IMAGE), Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES));

        if (photoFile != null) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
            startActivityForResult(intent, REQUEST_CAMERA);
        }

        return (photoFile != null) ? photoFile.getAbsolutePath() : "";
    }

    private boolean prepareParamsOrDisplayError(@NonNull Map<String, String> params) {
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);

        String rawInputFullName = binding_.fullName.getText().toString();

        if (TextUtils.isEmpty(rawInputFullName)) {
            UIUtil.showGravitatedToast(getContext(), R.string.msg_err_empty_name,
                    Toast.LENGTH_SHORT, Gravity.BOTTOM, 0, 200);

            binding_.fullName.requestFocus();
            imm.showSoftInput(binding_.fullName, InputMethodManager.SHOW_IMPLICIT);

            return false;
        }

        final String originalFullName = getString(R.string.placeholder_2_space, user_.getFirstName(), user_.getLastName());
        final String fullName = rawInputFullName.replaceAll("\\s+", " ").trim();

        if (!originalFullName.equals(fullName)) {
            String[] nameParts = NameUtil.splitNameToParts(fullName, NAME_PARTS_COUNT, "");

            if (!Utils.validateAndPut(KEY_FIRST_NAME, nameParts[0],
                    new Validators.StringNoLessThan(1), params)) {

                UIUtil.showGravitatedToast(getContext(), R.string.msg_err_empty_name,
                        Toast.LENGTH_SHORT, Gravity.BOTTOM, 0, 200);

                binding_.fullName.requestFocus();
                binding_.fullName.selectAll();
                imm.showSoftInput(binding_.fullName, InputMethodManager.SHOW_IMPLICIT);

                return false;
            }

            params.put(KEY_LAST_NAME, nameParts[1]);
        }

        final String trimmedEmailNumber = binding_.eMail.getText().toString().trim();
        if (!user_.getEmail().equals(trimmedEmailNumber)) {
            if (!Utils.validateAndPut(KEY_EMAIL, trimmedEmailNumber, new Validators.Email(), params)) {
                UIUtil.showGravitatedToast(getContext(), R.string.msg_err_incorrect_email,
                        Toast.LENGTH_SHORT, Gravity.BOTTOM, 0, 200);

                binding_.eMail.requestFocus();
                binding_.eMail.selectAll();
                imm.showSoftInput(binding_.eMail, InputMethodManager.SHOW_IMPLICIT);

                return false;
            }
        }

        params.put(Const.KEY_AUTH_TOKEN, user_.getToken());

        return true;
    }

    private @NonNull ProfileEditableItems updateUserDataLocal(@NonNull Map<String, String> params) {
        ProfileEditableItems updatedFields = new ProfileEditableItems();

        if (params.containsKey(KEY_FIRST_NAME)) {
            assertTrue("Params surname must exist", params.containsKey(KEY_LAST_NAME));

            user_.setFirstName(params.get(KEY_FIRST_NAME));
            user_.setLastName(params.get(KEY_LAST_NAME));

            updatedFields.addEditedField( ProfileEditableItems.FULL_NAME );
        }

        if (params.containsKey(KEY_EMAIL)) {
            user_.setEmail(params.get(KEY_EMAIL));

            updatedFields.addEditedField( ProfileEditableItems.E_MAIL );
        }

        if (params.containsKey(KEY_PROFILE_PICTURE)) {
            user_.setImagePath(params.get(KEY_PROFILE_PICTURE));

            updatedFields.addEditedField( ProfileEditableItems.PICTURE );
        }

        UserDataPrefProvider.getInstance(getContext()).saveData(user_);

        return updatedFields;
    }

    private boolean uploadImageIfNeeded(@Nullable File imageFile, final Map<String, String> params) {
        if (imageFile == null || !imageFile.exists()) {
            return false;
        }

        final Context appContext = getContext().getApplicationContext();

        final String filePathOnServer = generateServerPartPath(user_.getId(), PROFILE_PICTURE_NAME_ON_SERVER);

        if (fileUploadObserver_!=null) {
            fileUploadObserver_.cleanTransferListener();
            ConfigEnvHolder.getInstance(getContext()).getTransferUtility().cancel(fileUploadObserver_.getId());
        }

        fileUploadObserver_ = ConfigEnvHolder.getInstance(getContext())
                .getTransferUtility().upload(ConfigEnvHolder.getInstance(getContext()).getBucketName(), filePathOnServer, imageFile);
        fileUploadObserver_.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                Log.d(LOG_TAG, "Transfer rec " + id + " state changed to " + state);

                switch (state) {
                    case IN_PROGRESS: {
                        break;
                    }

                    case COMPLETED: {
                        // Clear cache
                        MyVolley.getInstance(appContext).getImageCache().evictAll();

                        params.put(KEY_PROFILE_PICTURE, filePathOnServer);
                        editedFields_ = updateUserDataLocal(params);
                        updateUserProfileRequest_.sendRequest(params);

                        fileUploadObserver_.cleanTransferListener();
                        break;
                    }

                    case CANCELED:
                    case FAILED: {
                        fileUploadObserver_.cleanTransferListener();

                        break;
                    }
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {

            }

            @Override
            public void onError(int id, Exception ex) {

            }
        });

        return true;
    }

    private static String generateServerPartPath(@NonNull String userId, @NonNull String fileName) {
        final String partPath = new Uri.Builder().encodedPath(ROUTES.URL_UPLOAD_PROFILE_PICTURE_PART).appendPath(userId).appendPath(fileName).toString();
        if (!partPath.isEmpty() && partPath.charAt(0) == '/') {
            return partPath.substring(1, partPath.length());
        }
        else {
            return partPath;
        }
    }

    private ResumableUpdateUserProfile createUpdateProfileTask(@NonNull Context ctx) {
        return new ResumableUpdateUserProfile(ctx, new WebServiceCoordinator.ResponseListener() {
            @Override
            public void onPreExecute() {
                binding_.saveChanges.setEnabled(false);
            }

            @Override
            public void onDataReady(@NonNull JSONObject data) {
                listener_.onUserDataUpdated(editedFields_);
            }

            @Override
            public void onWebRequestError(@NonNull VolleyError ex) {
//                setWaitingState(false);
                binding_.saveChanges.setEnabled(true);
                processResponseError(ex);
            }
        });
    }

    private void processResponseError(@NonNull final VolleyError ex) {
        int statusCode = (ex instanceof NoConnectionError) ? NetworkUtil.NET_NO_CONNECTION :
                (ex.networkResponse != null) ? ex.networkResponse.statusCode : NetworkUtil.NET_ERROR_GENERAL;
        Log.e(LOG_TAG, "\tStatus code: " + statusCode);
        String serverErrorJSON = (ex.networkResponse != null) ? VolleyJSONRequestHelper.parseAnyJSONResponse(ex.networkResponse).result.toString() : "";
        Log.e(LOG_TAG, "\tError as JSON: " + serverErrorJSON);

        switch (statusCode) {
            case NetworkUtil.NET_NO_CONNECTION: {
                NetworkUtil.showNetworkSettingsDialog(getContext());
                break;
            }

            default: {
//                String serverErrorJSON = (ex.networkResponse != null) ? VolleyJSONRequestHelper.parseAnyJSONResponse(ex.networkResponse).result.toString() : "";
//
//                Log.e(LOG_TAG, "\tError as JSON: " + serverErrorJSON);
            }
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnUserProfileFragmentListener {
        void onUserDataUpdated(@NonNull ProfileEditableItems editedItems);
    }
}
