package com.example.profile;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;

import java.util.Map;

import com.example.R;
import com.example.common.Notifications;
import com.example.common.UIUtil;
import com.example.common.Utils;
import com.example.databinding.ActivityProfileBinding;
import com.example.location.MyLocationSender;

public class ProfileActivity extends AppCompatActivity
        implements UserProfileFragment.OnUserProfileFragmentListener {
    private static final String LOG_TAG = ProfileActivity.class.getSimpleName();

    private ActivityProfileBinding binding_;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding_ = DataBindingUtil.setContentView(this, R.layout.activity_profile);

        setSupportActionBar(binding_.toolbarHolderBinder.appToolbar);

        tuneHomeButton();

        putSettingsFragmentWithCaption();
    }

    /**
     * Dispatch onStart() to all fragments.  Ensure any created loaders are
     * now started.
     */
    @Override
    protected void onStart() {
        super.onStart();

        MyLocationSender.getInstance(this).start();
    }

    /**
     * This is the fragment-orientated version of {@link #onResume()} that you
     * can override to perform operations in the Activity at the same point
     * where its fragments are resumed.  Be sure to always call through to
     * the super-class.
     */
    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();

        final IntentFilter actionsFilter = new IntentFilter(Notifications.Actions.AUTH_TOKEN_CHANGED);

        LocalBroadcastManager.getInstance(this).registerReceiver(notificationsBroadcastReceiver_,
                actionsFilter);
    }

    /**
     * Dispatch onPause() to fragments.
     */
    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(notificationsBroadcastReceiver_);

        super.onPause();
    }

    @Override
    protected void onStop() {

        MyLocationSender.getInstance(this).stop();

        super.onStop();
    }

    private void putSettingsFragment() {
        final FragmentManager fm = getSupportFragmentManager();

        UserProfileFragment userProfile = (UserProfileFragment)fm.findFragmentByTag(UserProfileFragment.FRAGMENT_TAG);
        if (userProfile == null) {
            userProfile = UserProfileFragment.newInstance();
            fm.beginTransaction()
                    .add(R.id.fragment_container, userProfile, userProfile.getFragmentTag())
                    .commit();
        }
    }

    private void putSettingsFragmentWithCaption() {
        binding_.toolbarHolderBinder.caption.setText(R.string.label_profile_settings);
        putSettingsFragment();
    }

    private void tuneHomeButton() {
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
    }

//********************************************************************************************************
// BroadcastReceiver with handler
//--------------------------------------------------------------------------------------------------------
    private final BroadcastReceiver notificationsBroadcastReceiver_ = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //Handle action
            final String action = intent.getAction();
            Log.d(LOG_TAG, "Received action = " + action);
			
            @SuppressWarnings("unchecked")
            final Map<String, String> args = (Map<String, String>)intent.getSerializableExtra(Notifications.KEY_PARAMS);
            handleBroadcast(action, args);
        }
    }; //BroadcastReceiver

    private void handleBroadcast(@NonNull final String action, @NonNull Map<String, String> params) {
        switch (action) {

            case Notifications.Actions.AUTH_TOKEN_CHANGED: {
                UIUtil.showGravitatedToast(this, R.string.msg_err_unauthorized, Toast.LENGTH_LONG, Gravity.BOTTOM, 0, 200);
                Utils.cleanUpAndRestartApp(this);

                break;
            }

//            case Notifications.Actions.INCOMING_CALL_DECLINED: Declined is handled in video stream
//            case Notifications.Actions.NEW_INTERVENTION: // handled in InterventionsFragment

            default:
                throw new UnsupportedOperationException("BUG: No handler defined for action " + action);
        }
    } //handleBroadcast

//**********************************************************************************************
// UserProfileFragment.OnUserProfileFragmentListener
//----------------------------------------------------------------------------------------------

    @Override
    public void onUserDataUpdated(@NonNull ProfileEditableItems editedItems) {
        UIUtil.showGravitatedToast(this, R.string.msg_profile_updated, Toast.LENGTH_SHORT, Gravity.BOTTOM, 0, 200);
        EventBus.getDefault().postSticky(editedItems);

        finish();
    }
}
