package com.example.profile;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.json.JSONObject;

import java.util.Map;

import com.example.network.AbsResumableNetRequest;
import com.example.network.ROUTES;
import com.example.network.WebServiceCoordinator;

/**
 * Created for demo on 27/01/17.
 */
class ResumableUpdateUserProfile extends AbsResumableNetRequest {
    private static final String TAG_UPDATE_PROFILE_DATA = "tag_update_profile_data";

    ResumableUpdateUserProfile(@NonNull Context ctx, @Nullable WebServiceCoordinator.ResponseListener delegate) {
        super(ctx, delegate);

        method_ = WebServiceCoordinator.Method.POST;
        url_ = ROUTES.URL_PROFILE_UPDATE;
    }

    void sendRequest(@NonNull Map<String, String> params) {
        requestBody_ = new JSONObject(params);

        start();
    }

    @Override
    public Object getTag() {
        return TAG_UPDATE_PROFILE_DATA;
    }
}
